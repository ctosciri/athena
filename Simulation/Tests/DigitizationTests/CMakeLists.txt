################################################################################
# Package: DigitizationTests
################################################################################

# Declare the package name:
atlas_subdir( DigitizationTests )

atlas_install_scripts( scripts/*.sh test/*.sh )
